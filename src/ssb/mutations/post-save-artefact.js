const pick = require('lodash.pick')
module.exports = function PostSaveArtefact (sbot) {
  return function postSaveArtefact (input, cb) {
    const { id, type, blob } = input
    const T = buildTransformation(input)

    if (id) {
      sbot.artefact.update(id, T, (err, _) => {
        if (err) cb(err)
        else cb(null, id)
      })
      return
    }

    if (!type || !blob) cb(new Error('Type and blob are required for creating a new artefact'))

    sbot.artefact.create(type, blob, T, (err, artefactId) => {
      if (err) cb(err)
      else cb(null, artefactId)
    })
  }
}

function buildTransformation (input) {
  const T = {}

  Object.entries(input).forEach(([key, value]) => {
    switch (key) {
      case 'id': return
      case 'type': return
      case 'blob': return

      case 'tombstone':
        T[key] = pick(value, ['date', 'reason'])
        T[key].date = Number(T[key].date)
        // graphql only allows 32bit signed Ints
        // so we're passing a Date and converting it to Int for ssb
        return

      case 'duration':
        T[key] = parseInt(value)
        return

      case 'authors':
        T[key] = {}
        if (value.add && value.add.length) T[key].add = value.add
        if (value.remove && value.remove.length) T[key].remove = value.remove
        return

      default:
        T[key] = value
    }
  })
  return T
}
